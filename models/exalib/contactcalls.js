'use strict';
const	bodyParser = require('body-parser');
const	hpp = require('hpp');

const	validate = require('./validate');
const	db = require('./contactdb');
const	mail = require('./mail');

const	SERVER_MAIL = 'noreply@cry.in';
const	contactDb = db.contactDb;

// create route object.
function newRoute(method, url, callback, next) {
	if (!next)
		next = function () {}
	return {
		'method': method,
		'url': url,
		'call': callback,
		'next': next
	};
}

// for reusability purpose this function has been created to insert data from Emergency form.
// must be called in all routes contains Emergency form
function emergencyCallback(req, res, url) {
		let	userData = null;
		let	valid = null;

		res.setHeader('Content-Type', 'application/json');
		if (!req.body) {
			return res.redirect(url);
		}
		else {
			userData = req.body;
			valid = validate.validateAll([
				{ 'type': 'email', 'name': 'email', 'value': userData.email },
				{ 'type': 'text', 'name': 'target', 'value': userData.target },
				{ 'type': 'text', 'name': 'Threat type', 'value': userData.threatType },
				{ 'type': 'name', 'name': 'text', 'value': userData.userType }
			]);
			console.log(valid);
			if (valid.isError === true)
				return res.redirect(url);
			else {
				contactDb.insertEmergencyMessage(userData, function (err) {
					console.log(err);
					return res.redirect(url);
				});
			}
		}

}

const	routes = [
	newRoute('get', '/', function (req, res, next) {
		/*contactDb.find({ 'email': 'ussef@email.info' }, function (err, user) {
			if (err) {
				console.log(err);
				return ;
			}
			if (user) {
				user.messages = undefined;
				res.cookie('user', JSON.stringify(user));
			}
			res.render('contact-us');
		});*/
		res.render('contact-us');
	}),
	newRoute('get', '/admin', function (req, res) {
		contactDb.all(function (err, users) {
			if (err)
				console.log(err);
			else
				res.render('admin_messages', { 'usersArray': users });
		});
	}),
	newRoute('post', '/', function (req, res, next) {
		let	userObj = null;
		let	valid = null;

		res.setHeader('Content-Type', 'application/json');
		if (!req.body) {
			console.log('just another error we must handle it');
			res.redirect('/');
		}
		else {
			userObj = req.body;
			valid = validate.validateAll([
				{ 'type': 'email', 'name': 'email', 'value': userObj.email },
				{ 'type': 'name', 'name': 'fname', 'value': userObj.fname },
				{ 'type': 'name', 'name': 'lname', 'value': userObj.lname },
				{ 'type': 'name', 'name': 'subject', 'value': userObj.subject },
				{ 'type': 'text', 'name': 'text', 'value': userObj.text },
				{ 'type': 'role', 'name': 'role', 'value': userObj.role }
			]);
			console.log(valid);
			if (valid.isError === true) {
				return next();
			}
			userObj.from = 'user';
			userObj.to = 'admin';
			userObj.status = 'open';
			contactDb.insertMessage(userObj, function (err) {
				if (err.isError === false) {
					return next();
					//mail.send({ from: SERVER_MAIL, to: userObj.email, subject: userObj.subject, text: userObj.text }, function () {
					//	return next();
					//});
				}
				else
					return next();
			});
		}
	}, function (req, res) {
		res.redirect('/');
	}),
	newRoute('post', '/admin', function (req, res, next) {
		if (req.isAuthenticated() === false)
			return res.redirect('/login');
		let	userObj = null;
		let	valid = null;

		res.setHeader('Content-Type', 'application/json');
		if (!req.body) {
			console.log('the body is null or undefined !!, check if you\'r express use body-parser');
			return next();
		}
		else {
			userObj = req.body;	
			valid = validate.validateAll([
				{ 'type': 'email', 'name': 'email', 'value': userObj.to },
				{ 'type': 'name', 'name': 'subject', 'value': userObj.subject },
				{ 'type': 'text', 'name': 'text', 'value': userObj.text }
			]);
			if (valid.isError === true)
				return (next());
				contactDb.find({ 'email': userObj.to }, function(err, user) {
					if (err) {
						console.log(err);
						return next();
					}
					else if (user === null) {
						console.log('user not found');
					return next();
				}
				contactDb.insertMessage({
					'fname': user.fname,
					'lname': user.lname,
					'role': user.role,
					'email': user.email,
					'subject':  userObj.subject,
					'status': 'open',
					'text': userObj.text,
					'from': 'admin',
					'to': 'user'
				},
				function (err) {
					console.log(err);
					if (err.isError === false) {
						mail.send({ from: SERVER_MAIL, to: user.email, subject: userObj.subject, text: userObj.text }, function (err) {
							return next();
						});
					}
					else
						return next();
				});
			});
		}
	}, function (req, res) {
		res.redirect('/admin');
	}),
	newRoute('post', '/', function (req, res) {
		emergencyCallback(req, res, '/');
	})
];

module.exports = routes;
