'use strict';
const	express = require('express');
const	bodyParser = require('body-parser');
const	mongoose = require('mongoose');
const	path = require('path');
const	hpp = require('hpp');
const	favicon  = require('serve-favicon');

const	lib = require('./models/exalib/lib');
const	contactCalls = require('./models/exalib/contactcalls');

const	app = express();

app.use(bodyParser.urlencoded({ extended: false }));

app.use(bodyParser.json());

app.use(hpp());

app.use(favicon(path.join('public/images', 'favicon.ico')));

/* set the absolute path to serve static files */
app.use('/', express.static((__dirname, 'public')));

/* set ejs module config */
app.set('view engine', 'ejs');

/* change the default views directory to 'pages' */
app.set('views', path.join(__dirname, 'pages'));

contactCalls.forEach(function (route) {
	if (lib.isFunction(route.next) === false)
		route.next = function () {}
	switch (route.method) {
		case 'get': {
			app.get(route.url, route.call, route.next);
		} break ;
		case 'post': {
			app.post(route.url, route.call, route.next);
		} break ;
	}
});

app.listen(1337, function () {
	console.log('server start listening at port 1337');
});
